$(function() {
  c = $("<canvas>")[0];
  h = c.height = 16;
  w = c.width = 16;
  d = c.getContext('2d');
  worker = null;
  $("#reset").css({width: '300px', height: '300px'});
  $("#reset").on('click', function() { alarm_stop(); alarm_set(); });
  $("#stop").on('click', function() { alarm_stop(); });
});

function favico(color) {
  d.fillStyle = color;
  d.fillRect(0,0,w,h);
  $('#favicon').attr('href', c.toDataURL());
}

function alarm_ring() {
  var b =  $("#beeps")[0];
  b.loop = true;
  b.volume = 0.1;
  b.play();
  worker = new Worker('worker.js');
  counter = 0;
  worker.addEventListener('message', function(msg) {
    counter++;
    favico(counter % 2 ? "red" : "yellow");
    window.document.title = counter % 2 ? "HEY" : "\u200E";
  });
}

function alarm_stop() {
  var b = $("#beeps")[0];
  b.pause();
  b.currentTime = 0;
  if (worker != null) {
    worker.terminate();
  }
  favico("green");
  window.document.title = "Alarm";
}

function get_time() {
  return new Date().getHours();
}

function alarm_set() {
  old_time = get_time();
  setTimeout(alarm_check, 1000);
}

function alarm_check() {
  $("#debug").text('old_time = ' + old_time + ', now = ' + new Date());
  if (old_time != get_time()) {
    alarm_ring();
  }
  else {
    setTimeout(alarm_check, 1000);
  }
}
